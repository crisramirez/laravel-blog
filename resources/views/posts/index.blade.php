@extends('layouts.master')

@section('content')

    <div class="col-sm-8 blog-main">

        <main role="main" class="container">

            <div class="row">

                <div class="col-sm-8 blog-main">

                    @foreach($posts as $post)
                        @include('posts.post')
                    @endforeach

                </div>

            </div><!-- /.row -->

        </main><!-- /.container -->

    </div>

@endsection