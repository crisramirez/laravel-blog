@extends('layouts.master')

@section('content')

    <div class="col-sm-8 blog-main">

        <h1>Create a post</h1>

        <form method="post" action="/posts">

            {{ csrf_field() }}

            <div class="form-group">
                <label for="postTitle">Title:</label>
                <input type="text" class="form-control" id="postTitle" name="title">
            </div>

            <div class="form-group">
                <label for="postBody">Body</label>
                <textarea class="form-control" id="postBody" name="body"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Publish</button>
            </div>

        </form>

        @include('layouts.errors')

    </div>

@endsection